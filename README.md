# VPC

VPCs for the various places to put them (AWS, GCP, etc).

## AWS

All you have to do to get this working is create your own bucket:

`aws s3 mb s3://<bucket-name>`

and then just like add the bucket ARN to the flow logs section
